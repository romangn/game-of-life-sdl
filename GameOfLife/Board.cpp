#include "Board.h"

/*
Constructor for the board
initializes the data members
*/
Board::Board()
{
	height = HBOARD;
	width = WBOARD;
	std::vector<std::vector<Cell>> temp(height, std::vector<Cell>(width, Cell()));
	board = temp;
	setUpCells();
}

/*
Initial set up of the cells on the board
*/
void Board::setUpCells()
{
	const int offset = 2;
	int yPos = 0;
	for (size_t i = 0; i < board.size(); i++)
	{
		int xPos = 0;
		if (i > 0)
			yPos += 10;
		for (size_t j = 0; j < board[i].size(); j++)
		{
			board[i][j].setXPos(xPos + offset);
			board[i][j].setYPos(yPos + offset);
			xPos += 10;
		}
	}
}

/*
Setting up the neighbours of each cell on the grid
*/
void Board::Draw()
{
	for (size_t i = 0; i < board.size(); i++)
	{
		for (size_t j = 0; j < board[i].size(); j++)
		{			
			board[i][j].setNumOfNeghbours(countNeighbours(i, j));
		}
	}
	updateData(board);
}

/*
Checks if given width and height position are within bounds
@param h the height position
@param w the width position
@return true if in bounds else false
*/
bool Board::isInBounds(size_t h, size_t w)
{
	if (h >= height || w >= width) { return false; }
	return true;
}

/*
Counts the neighbours in the nearby cells
@param h the height location of the board
@param w - the width location of the board
@return the number of nearby neighbours
*/
size_t Board::countNeighbours(size_t h, size_t w)
{
	size_t tempCounter = 0;
	if (isInBounds(h - 1, w))
	{
		if (board[h - 1][w].checkIfTheCellIsAlive()) { tempCounter++; }
	}
	if (isInBounds(h - 1, w - 1))
	{
		if (board[h - 1][w - 1].checkIfTheCellIsAlive()) { tempCounter++; }
	}
	if (isInBounds(h - 1, w + 1))
	{
		if (board[h - 1][w + 1].checkIfTheCellIsAlive()) { tempCounter++; }
	}
	if (isInBounds(h, w - 1))
	{
		if (board[h][w - 1].checkIfTheCellIsAlive()) { tempCounter++; }
	}
	if (isInBounds(h, w + 1))
	{
		if (board[h][w + 1].checkIfTheCellIsAlive()) { tempCounter++; }
	}
	if (isInBounds(h + 1, w))
	{
		if (board[h + 1][w].checkIfTheCellIsAlive()) { tempCounter++; }
	}
	if (isInBounds(h + 1, w - 1))
	{
		if (board[h + 1][w - 1].checkIfTheCellIsAlive()) { tempCounter++; }
	}
	if (isInBounds(h + 1, w + 1))
	{
		if (board[h + 1][w + 1].checkIfTheCellIsAlive()) { tempCounter++; }
	}
	return tempCounter;
}

/*
For each item on the grid checks if the item is alive, how many live neighbours it has and sets the cell status
depending on it.
@param vec the vector of cells to check
*/
void Board::updateData(std::vector<std::vector<Cell>>& vec)
{
	for (size_t i = 0; i < vec.size(); i++)
	{
		for (size_t j = 0; j < vec[i].size(); j++)
		{
			bool result = false;;
			if (vec[i][j].checkIfTheCellIsAlive() && vec[i][j].getNumNeighbours() < 2)
			{
				result = false;
			}
			else if ((vec[i][j].checkIfTheCellIsAlive() && vec[i][j].getNumNeighbours() == 2) ||
				(vec[i][j].checkIfTheCellIsAlive() && vec[i][j].getNumNeighbours() == 3))
			{
				result = true;
			}
			else if (vec[i][j].checkIfTheCellIsAlive() && vec[i][j].getNumNeighbours() > 3)
			{
				result = false;
			}
			else if ((!vec[i][j].checkIfTheCellIsAlive()) && vec[i][j].getNumNeighbours() == 3)
			{
				result = true;
			}
			vec[i][j].setLiveliness(result);
		}
	}
}
