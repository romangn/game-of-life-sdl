#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include "Board.h"
 

class Core
{
public:
	Core() { paused = true; quit = false; }
	void draw();
	Board board;
private:
	const int SCREEN_WIDTH = 880;
	const int SCREEN_HEIGHT = 600;
	bool paused;
	bool quit;
	SDL_Window* gWindow = NULL;
	SDL_Renderer* gRenderer = NULL;
	bool initialize();
	void close();
	void handleMouseClick(int x, int y);
	void drawBoardWithoutCells();
	void fillCellsWithColor();
	void handleInput();
};
