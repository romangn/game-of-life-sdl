#pragma once
#include <string>
#include <SDL.h>

class Cell
{
public:
	Cell() { isAlive = false; rect.h = CELLHEIGHT; rect.w = CELLHEIGHT;}
	void setNumOfNeghbours(size_t number);
	bool checkIfTheCellIsAlive();
	size_t getNumNeighbours();
	void setLiveliness(bool status);
	void setXPos (int x);
	void setYPos(int y);
	SDL_Rect getRect();
	int getXposition();
	int getYposition();
	bool checkCoordinatesWithinBounds(int x, int y);
private:
	bool isAlive;
	size_t numOfNeighbours;
	SDL_Rect rect;
	static const int CELLHEIGHT = 7;
	static const int CELLWIDTH = 7;
};
