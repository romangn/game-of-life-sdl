#include "Core.h"

/* Initializes the SDL, creates window and renderer.
@return false if any initialization failed, else true
*/
bool Core::initialize()
{
	bool success = true;
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "SDL could not be initialized. SDL_Error: " << SDL_GetError() << std::endl;
		success = false;
	}
	else
	{
		gWindow = SDL_CreateWindow("Game of Life", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH,
			SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (!gWindow)
		{
			std::cout << "Window could not be created. SDL_Error: " << SDL_GetError() << std::endl;
			success = false;
		}
		else
		{
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
			if (!gRenderer)
			{
				std::cout << "Renderer could not be created. SDL_Error" << SDL_GetError() << std::endl;
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					std::cout << "SDL image could not be initialized! SDL_Image_Error: " << IMG_GetError() << std::endl;
					success = false;
				}
			}
		}
	}
	return success;
}

/*
Cleans up after the programme and closes SDL subsystems
*/
void Core::close()
{
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	IMG_Quit();
	SDL_Quit();
}

/*
The main loop of the game. If the initialization was successful, draws objects to the screen while the game is still running.
*/
void Core::draw()
{
	const char* info = "To play the Game of Life please use the left mouse button to place the cells and press space "
		"when you are ready to see the simulation. Press space any time to pause the simulation.";
	if (!initialize())
	{
		std::cout << "Failed to initialize" << std::endl;
	}
	else
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "How to play", info, gWindow);
		while (!quit)
		{
			handleInput();
			drawBoardWithoutCells();
			if (!paused)
			{
				board.Draw();
			}
			fillCellsWithColor();
			SDL_RenderPresent(gRenderer);
		}
	}
	close();
}

/*
Handles keyboard and mouse inputs
*/
void Core::handleInput()
{
	SDL_Event e;
	while (SDL_PollEvent(&e) != 0)
	{
		if (e.type == SDL_QUIT)
		{
			quit = true;
		}
		if (e.type == SDL_KEYDOWN)
		{
			if (e.key.keysym.sym == SDLK_SPACE)
			{
				if (paused)
					paused = false;
				else
					paused = true;
			}
		}
		if (e.type == SDL_MOUSEBUTTONDOWN)
		{
			if (e.button.button == SDL_BUTTON_LEFT)
			{
				int x, y;
				SDL_GetMouseState(&x, &y);
				handleMouseClick(x, y);
			}
		}
	}
}

/*
Attempts to calculate the position where the object was clicked and revive it. 
*/
void Core::handleMouseClick(int x, int y)
{
	if (x < 0 || y < 0)
		return;
	if (paused)
	{
		for (size_t i = 0; i < board.board.size(); i++)
		{
			for (size_t j = 0; j < board.board[i].size(); j++)
			{
				if (board.board[i][j].checkCoordinatesWithinBounds(x, y)) 
					board.board[i][j].setLiveliness(true);
			}
		}
	}
}

/*
Draws the empty grid
*/
void Core::drawBoardWithoutCells()
{
	SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(gRenderer);
	SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	for (int i = 0; i < SCREEN_WIDTH; i++)
	{
		if (i % 10 == 0)
			SDL_RenderDrawLine(gRenderer, i, 0, i, SCREEN_HEIGHT);
	}
	for (int i = 0; i < SCREEN_HEIGHT; i++)
	{
		if (i % 10 == 0)
			SDL_RenderDrawLine(gRenderer, 0, i, SCREEN_WIDTH, i);
	}
	SDL_RenderDrawLine(gRenderer, 0, 550, 880, 550);
}

/*
Fills grid objects with colour depending on its status
*/
void Core::fillCellsWithColor()
{
	for (size_t i = 0; i < board.board.size(); i++)
	{
		for (size_t j = 0; j < board.board[i].size(); j++)
		{
			if (board.board[i][j].checkIfTheCellIsAlive())
			{
				SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
				SDL_RenderFillRect(gRenderer, &board.board[i][j].getRect());
			}
			else
			{
				SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
				SDL_RenderFillRect(gRenderer, &board.board[i][j].getRect());
			}
		}
	}
}