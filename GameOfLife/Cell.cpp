#include "Cell.h"

/*
Sets number of neighbours the cell has
@param number the number of neighbours
*/
void Cell::setNumOfNeghbours(size_t number)
{
	numOfNeighbours = number;
}

/*
Checks if the cell is still alive
@returns true if alive else false
*/
bool Cell::checkIfTheCellIsAlive()
{
	return isAlive;
}

/*
Gets the number of neighbours the cell has
*/
size_t Cell::getNumNeighbours()
{
	return numOfNeighbours;
}

/*
Sets the status of the cell
@param status status to set
*/
void Cell::setLiveliness(bool status)
{
	isAlive = status;
}

/*
Sets the x position of the rectangle object wihtin a cell
@param x the position to set
*/
void Cell::setXPos(int x)
{
	rect.x = x;
}

/*
Sets the y position of the rectangle object wihtin a cell
@param y the position to set
*/
void Cell::setYPos(int y)
{
	rect.y = y;
}

/*
Gets the rectangle belonging to the current cell
@return rectangle
*/
SDL_Rect Cell::getRect()
{
	return rect;
}

/*
Gets the x position of the rectangle that belongs to the cell
@return x position of the rectangle that belongs to the cell 
*/
int Cell::getXposition()
{
	return rect.x;
}

/*
Gets the x position of the rectangle that belongs to the cell
@return y position of the rectangle that belongs to the cell
*/
int Cell::getYposition()
{
	return rect.y;
}

/*
Checks if the given coordinates are within the bound of rectangle and its width/height
@param x the x coordinate received from the mouse click
@param y the y coordinate received from the mouse click
@return true if the coordinate is within bounds of the rectangle, false if not
*/
bool Cell::checkCoordinatesWithinBounds(int x, int y)
{
	bool result = true;
	if ((x < getXposition() - 4) || (x > getXposition() + 4))
	{
		result = false; 
	}
	if (result)
	{
		if ((y < getYposition() - 4) || (y > getYposition() + 4))
			result = false;
	}
	return result;
}
