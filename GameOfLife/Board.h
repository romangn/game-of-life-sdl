#pragma once
#include <vector>
#include <iostream>
#include "Cell.h"

class Board
{
public:
	
	Board();
	void Draw();
	std::vector<std::vector<Cell>> board;
private:
	const size_t HBOARD = 60;
	const size_t WBOARD = 88;
	size_t height;
	size_t width;
	size_t countNeighbours(size_t h, size_t w);
	bool isInBounds(size_t h, size_t w);
	void updateData(std::vector<std::vector<Cell>>& vec);
	void setUpCells();
};
